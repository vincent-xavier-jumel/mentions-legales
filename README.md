Mentions légales : 

-	Identité :
o	Nom 
o	Prénom 
-	Coordonnées : 
o	Adresse domicile 
o	Email 
o	Téléphone 
-	Mentions relatives à la PI : 
o	Citer les auteurs pour les reprises d’images ou de texte
-	Mentions relatives à l’hébergement du site 
o	Nom de l’hébergeur 
o	Raison sociale 
o	Adresse 
o	Numéro de téléphone 
-	Mentions liées aux sites d’information 
o	Nom du directeur de publication 
o	Nom du responsable de la rédaction 
o	Coordonnées de l’hébergeur du site 

Politique de confidentialité des données (cad mentions relatives au RGPD) : 

-	Identité et coordonnées de l’organisme responsable du traitement de données
-	Coordonnées du délégué à la protection des données (DPO), ou d’un point de contact sur les questions de protection des données personnelles
-	Base juridique du traitement de données (consentement de l’internaute, respect d’une obligation prévue par un texte, exécution d’un contrat, etc.)
-	Finalité des données collectées (pour prise de décisions automatisée, pour prévenir la fraude, parce que les informations sont requises par la réglementation, etc.)
-	Caractère obligatoire ou facultatif du recueil des données et les conséquences pour la personne en cas de non-fourniture des données
-	Destinataires ou catégories de destinataires des données
-	Durée de conservation des données
-	Droits de l’internaute : droit de refuser la collecte, droit d’accéder, de rectifier et d’effacer ses données, et droit de déposer une plainte auprès de la Cnil
-	Transfert de données à caractère personnel envisagés à destination d'un État n'appartenant pas à l'Union européen

Mentions relatives aux cookies : 

-	Cookies fonctionnels – comment c’est récolté - indiquer les finalités . 
-	Cookies non fonctionnels : consentement obligatoire 
o	Publicité 
o	Partage sur les réseaux sociaux 


Conditions générales d’utilisation : 

-	Présentation, description des finalités et du fonctionnement des services 
-	Dispositifs relatifs à la PI cad obligations des utilisateurs et éventuelles sanctions
o	Licence : droits et devoirs 
-	Règles délimitant la responsabilité de l’éditeur du site (responsable des contenus figurant sur le site internet) => Prévoir une décharge partielle de responsabilité de l’éditeur : 
o	Ne pas être tenu pour responsable du contenu republié par les tiers à partir d’un autre site 
o	Ne pas être tenu pour responsable de fausses déclarations, insultes, propos diffamants tenus sur son site, forum ou chat
o	Ne pas garantir un accès continu du site : si bug ou interruption du site pour assurer sa maintenance
o	Exposer la politique d’utilisation des données personnelles des visiteurs conformément aux dispositifs du RGPD 
-	Droit applicable et juridiction compétente 
o	R.312-1 CJA 
