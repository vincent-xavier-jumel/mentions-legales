// script.js

// Tableau des questions et des réponses possibles avec droits et obligations ajoutés
const questions = [
    {
        question: "Que souhaitez-vous protéger ?",
        options: [
            { text: "Un code source ou un logiciel", next: 1 },
            { text: "Du texte", next: 4 },
            { text: "Une ou des informations à destination du public", next: 5 }
        ]
    },
    {
        question: "Choisissez le type de licence pour le logiciel :",
        options: [
            { text: "Licences permissives", next: 2 },
            { text: "Licences avec obligation de réciprocité", next: 3 }
        ]
    },
    {
        question: "Choisissez une licence permissive :",
        options: [
            { 
                text: "Apache License 2.0", 
                license: "https://www.apache.org/licenses/LICENSE-2.0",
                rights: "Le logiciel peut être librement utilisé, reproduit, modifié, distribué ou vendu. Les développeurs peuvent librement combiner du code provenant de logiciels publiés sous les licences General Public License (à partir de la version 3) et GNU version 3",
                obligations: "Les logiciels Apache ne peuvent pas être redistribués sans attribution. Une copie de la licence doit être redistribuée avec tout logiciel Apache. Les contributions externes aux logiciels sont publiées selon les conditions de l'ASF sauf indication contraire explicite."
            },
            { 
                text: "BSD 2-Clause 'Simplified' License", 
                license: "https://opensource.org/licenses/BSD-2-Clause",
                rights: "Utilisation, modification, distribution",
                obligations: "Inclure une copie de la licence"
            },
            { 
                text: "BSD 3-Clause 'New' or 'Revised' License", 
                license: "https://opensource.org/licenses/BSD-3-Clause",
                rights: "Utilisation, modification, distribution",
                obligations: "Inclure une copie de la licence, indiquer les modifications, ne pas utiliser le nom des auteurs pour promotion"
            },
            { 
                text: "CeCILL-B-Free Software License Agreement", 
                license: "http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html",
                rights: "Utilisation, modification, distribution",
                obligations: "Inclure une copie de la licence, indiquer les modifications"
            },
            { 
                text: "MIT License", 
                license: "https://opensource.org/licenses/MIT",
                rights: "Utilisation, modification, distribution",
                obligations: "Inclure une copie de la licence"
            }
        ]
    },
    {
        question: "Choisissez une licence avec obligation de réciprocité :",
        options: [
            { 
                text: "CeCILL Free Software License Agreement v2.1", 
                license: "http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html",
                rights: "Utilisation, modification, distribution",
                obligations: "Inclure une copie de la licence, indiquer les modifications, rendre le code source disponible"
            },
            { 
                text: "CeCILL-C Free Software License Agreement", 
                license: "http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html",
                rights: "Utilisation, modification, distribution",
                obligations: "Inclure une copie de la licence, indiquer les modifications, rendre le code source disponible, partager sous la même licence"
            },
            { 
                text: "GNU General Public License v3.0 or later", 
                license: "https://www.gnu.org/licenses/gpl-3.0.html",
                rights: "Utilisation, modification, distribution",
                obligations: "Inclure une copie de la licence, rendre le code source disponible, partager sous la même licence"
            },
            { 
                text: "GNU Lesser General Public License v3.0 or later", 
                license: "https://www.gnu.org/licenses/lgpl-3.0.html",
                rights: "Utilisation, modification, distribution, inclusion dans des œuvres propriétaires",
                obligations: "Inclure une copie de la licence, rendre le code source disponible, partager les modifications sous la même licence"
            },
            { 
                text: "GNU Affero General Public License v3.0 or later", 
                license: "https://www.gnu.org/licenses/agpl-3.0.html",
                rights: "Utilisation, modification, distribution",
                obligations: "Inclure une copie de la licence, rendre le code source disponible, partager sous la même licence, rendre le code source disponible pour les utilisateurs via un réseau"
            },
            { 
                text: "Mozilla Public License 2.0", 
                license: "https://www.mozilla.org/en-US/MPL/2.0/",
                rights: "Utilisation, modification, distribution, inclusion dans des œuvres propriétaires",
                obligations: "Inclure une copie de la licence, rendre le code source des fichiers modifiés disponible"
            },
            { 
                text: "Eclipse Public License 2.0", 
                license: "https://www.eclipse.org/legal/epl-2.0/",
                rights: "Utilisation, modification, distribution",
                obligations: "Inclure une copie de la licence, rendre le code source disponible, indiquer les modifications"
            },
            { 
                text: "European Union Public License 1.2", 
                license: "https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12",
                rights: "Utilisation, modification, distribution",
                obligations: "Inclure une copie de la licence, rendre le code source disponible, indiquer les modifications"
            }
        ]
    },
    {
        question: "Choisissez une licence Creative Commons :",
        options: [
            { 
                text: "CC0 1.0", 
                license: "https://creativecommons.org/publicdomain/zero/1.0/",
                rights: "Utilisation, modification, distribution, sans restriction",
                obligations: "Aucune"
            },
            { 
                text: "CC BY 4.0", 
                license: "https://creativecommons.org/licenses/by/4.0/",
                rights: "Utilisation, modification, distribution",
                obligations: "Créditer l'auteur"
            },
            { 
                text: "CC BY-SA 4.0", 
                license: "https://creativecommons.org/licenses/by-sa/4.0/",
                rights: "Utilisation, modification, distribution",
                obligations: "Créditer l'auteur, partager sous la même licence"
            },
            { 
                text: "CC BY-NC 4.0", 
                license: "https://creativecommons.org/licenses/by-nc/4.0/",
                rights: "Utilisation, modification, distribution, non-commercial",
                obligations: "Créditer l'auteur"
            },
            { 
                text: "CC BY-ND 4.0", 
                license: "https://creativecommons.org/licenses/by-nd/4.0/",
                rights: "Utilisation, distribution",
                obligations: "Créditer l'auteur, pas de modifications",
                note: "Cette licence est incompatible avec les communs numériques et n'est donc pas recommandée."
            },
            { 
                text: "CC BY-NC-SA 4.0", 
                license: "https://creativecommons.org/licenses/by-nc-sa/4.0/",
                rights: "Utilisation, modification, distribution, non-commercial",
                obligations: "Créditer l'auteur, partager sous la même licence",
                note: "Cette licence est incompatible avec les communs numériques et n'est donc pas recommandée."
            },
            { 
                text: "CC BY-NC-ND 4.0", 
                license: "https://creativecommons.org/licenses/by-nc-nd/4.0/",
                rights: "Utilisation, distribution, non-commercial",
                obligations: "Créditer l'auteur, pas de modifications",
                note: "Cette licence est incompatible avec les communs numériques et n'est donc pas recommandée."
            }
        ]
    },
    {
        question: "Choisissez le type de licence pour l'information publique :",
        options: [
            { 
                text: "Licence Ouverte version 2.0", 
                license: "https://www.etalab.gouv.fr/licence-ouverte-open-licence",
                rights: "Utilisation, modification, distribution",
                obligations: "Mentionner la source"
            },
            { 
                text: "ODC Open Database License (ODbL) version 1.0", 
                license: "https://opendatacommons.org/licenses/odbl/1-0/",
                rights: "Utilisation, modification, distribution",
                obligations: "Mentionner la source, partager sous la même licence, rendre le code source disponible"
            }
        ]
    }
];

// Initialisation de la variable de l'étape courante
let currentStep = 0;

// Fonction pour afficher une question
function showQuestion(step) {
    const container = document.getElementById('question-container');
    container.innerHTML = ''; // Vider le conteneur

    // Afficher la question
    const question = document.createElement('div');
    question.className = 'question';
    question.innerText = questions[step].question;
    container.appendChild(question);

    // Afficher les options de réponse
    questions[step].options.forEach(option => {
        const button = document.createElement('button');
        button.innerText = option.text;
        button.onclick = () => {
            if (option.next !== undefined) {
                // Afficher la question suivante si disponible
                showQuestion(option.next);
            } else if (option.license) {
                // Afficher le résultat final si une licence est spécifiée
                showResult(option.license, option.rights, option.obligations, option.note);
            }
        };
        container.appendChild(button);
    });
}

// Fonction pour afficher le résultat final
function showResult(licenseUrl, rights, obligations, note) {
    const container = document.getElementById('result-container');
    container.innerHTML = '<h2>Licence recommandée :</h2>';
    
    const link = document.createElement('a');
    link.href = licenseUrl;
    link.innerText = licenseUrl;
    link.target = '_blank';
    container.appendChild(link);

    // Affichage des droits
    const rightsDiv = document.createElement('div');
    rightsDiv.innerHTML = `<h3>Droits :</h3><p>${rights}</p>`;
    container.appendChild(rightsDiv);

    // Affichage des obligations
    const obligationsDiv = document.createElement('div');
    obligationsDiv.innerHTML = `<h3>Obligations :</h3><p>${obligations}</p>`;
    container.appendChild(obligationsDiv);

    // Affichage de la note si disponible
    if (note) {
        const noteDiv = document.createElement('div');
        noteDiv.innerHTML = `<h3>Note :</h3><p>${note}</p>`;
        container.appendChild(noteDiv);
    }
}

// Afficher la première question
showQuestion(currentStep);
